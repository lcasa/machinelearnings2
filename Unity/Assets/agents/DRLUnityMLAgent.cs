﻿using System;
using System.Collections.Generic;
using System.Linq;
using DRLEnv.Contracts;
using MLAgents;
using UnityEngine;

namespace DRLEnv.Agents
{
    public class DRLUnityMLAgent<TInformationSet, TActionValue> : Agent, IAgent<TInformationSet,
        TActionValue>
        where TInformationSet : IInformationSet<TInformationSet>
    {

        private float[] vectorizedInformationSet;
        private int[] availableActionsId;
        private int chosenActionId;

        private bool inDecision;
        private bool ready;
        
        public override void CollectObservations()
        {
            AddVectorObs(vectorizedInformationSet);
            var totalActions = brain.brainParameters.vectorActionSize[0];
            for (var i = 0; i < totalActions; i++)
            {
                if (!availableActionsId.Contains(i))
                {
                    SetActionMask(0, i);
                }
            }
        }

        public override void AgentAction(float[] vectorAction, string textAction)
        {
            chosenActionId = Mathf.FloorToInt(vectorAction[0]);
            ready = true;
        }

        public (TActionValue actionValue, bool ready) Act(int playerId,
            TInformationSet informationSet,
            IReadOnlyDictionary<int, TActionValue> availableActions)
        {
            if (!inDecision)
            {
                ready = false;
                inDecision = true;
                vectorizedInformationSet = informationSet.Vectorize();
                availableActionsId = availableActions.Keys.ToArray();
                RequestDecision();
//                ResetReward();
                return (default, false);
            }

            if (!ready)
            {
                return (default, false);
            }
            
            inDecision = false;
            ready = false;
            return (availableActions[chosenActionId], true);
        }

        public void InformActionResult(int playerId, IReadOnlyDictionary<int, float> rewards, IReadOnlyDictionary<int, float> scores, bool terminal)
        {
//            AddReward(rewards[playerId]);

            if (terminal)
            {
                Done();
                SetReward(scores[playerId]);
            }
        }
    }
}
