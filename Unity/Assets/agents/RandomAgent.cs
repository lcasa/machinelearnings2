using System.Collections.Generic;
using System.Linq;
using DRLEnv.Contracts;
using UnityEngine;

namespace DRLEnv.Agents
{
    public class RandomAgent<TInformationSet, TActionValue> : 
        IAgent<TInformationSet, TActionValue>
        where TInformationSet : IInformationSet<TInformationSet>
    {
        public (TActionValue actionValue, bool ready ) Act(int playerId,
            TInformationSet informationSet,
            IReadOnlyDictionary<int, TActionValue> availableActions)
        {
            var actions = availableActions.ToArray();

            return (actions[Random.Range(0, actions.Length)].Value, true);
        }

        public void InformActionResult(int playerId,
            IReadOnlyDictionary<int, float> rewards,
            IReadOnlyDictionary<int, float> scores,
            bool terminal)
        {
        }
    }
}