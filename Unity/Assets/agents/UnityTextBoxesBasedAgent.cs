using System;
using System.Collections.Generic;
using System.Linq;
using DRLEnv.Contracts;
using TMPro;
using UniRx;
using UniRx.Triggers;
using UnityEngine.UI;
using Zenject;

namespace DRLEnv.Agents
{
    public class UnityTextBoxesBasedAgent<TInformationSet, TActionValue> : IInitializable,
        IDisposable,
        IAgent<TInformationSet, TActionValue>
        where TInformationSet : IInformationSet<TInformationSet>
    {
        [Inject(Id = "LastRoundScore")]
        private TextMeshProUGUI lastRoundScoreText;

        [Inject(Id = "CurrentRoundScore")]
        private TextMeshProUGUI currentRoundScoreText;

        [Inject(Id = "AvailableActions")]
        private TextMeshProUGUI availableActionText;

        [Inject(Id = "CurrentPlayer")]
        private TextMeshProUGUI currentPlayerText;

        [Inject(Id = "InfoSet")]
        private TextMeshProUGUI infoSetText;

        [Inject]
        private TMP_InputField inputField;

        private readonly CompositeDisposable disposable = new CompositeDisposable();

        private int chosenActionId;

        private bool isReady;

        private Dictionary<int, float> transitionRewards;

        public (TActionValue actionValue, bool ready) Act(int playerId,
            TInformationSet informationSet,
            IReadOnlyDictionary<int, TActionValue> availableActions)
        {
            currentPlayerText.text = "Player : " + playerId;
            infoSetText.text = informationSet.ToString();
            availableActionText.text = DictionaryToString(availableActions);

            if (!isReady)
            {
                return (default, ready: isReady);
            }

            isReady = false;
            inputField.text = string.Empty;
            return !availableActions.ContainsKey(chosenActionId)
                ? (default, isReady)
                : (availableActions[chosenActionId], true);
        }

        public void InformActionResult(int playerId,
            IReadOnlyDictionary<int, float> rewards,
            IReadOnlyDictionary<int, float> scores,
            bool terminal)
        {
            if (terminal)
            {
                lastRoundScoreText.text = DictionaryToString(scores);
                currentRoundScoreText.text = string.Empty;
            }
            else
            {
                currentRoundScoreText.text = DictionaryToString(scores);
            }
        }

        private Subject<Unit> onEndEditSubject = new Subject<Unit>();
        
        public void Initialize()
        {
            inputField.onEndEdit.AddListener(_ => onEndEditSubject.OnNext(Unit.Default));
            currentRoundScoreText.text = string.Empty;
            lastRoundScoreText.text = string.Empty;
            
            onEndEditSubject
                .Subscribe(_ =>
                {
                    if (int.TryParse(inputField.text, out var result))
                    {
                        isReady = true;
                        chosenActionId = result;
                        return;
                    }

                    isReady = false;
                    inputField.text = string.Empty;
                    chosenActionId = -1;
                })
                .AddTo(disposable);
        }

        public void Dispose()
        {
            inputField.onEndEdit.RemoveAllListeners();
            disposable?.Dispose();
        }
        
        private static string DictionaryToString<T1, T2>(IReadOnlyDictionary<T1, T2> dict)
        {
            return string.Join(";", dict.Select(x => x.Key + "=" + x.Value));
        }
    }
}