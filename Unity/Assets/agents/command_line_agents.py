from machinelearnings2.contracts.agents import Agent
from machinelearnings2.contracts.information_sets import InformationSet


class CommandLineAgent(Agent):
    def act(self, agent_id: int, info_set: 'InformationSet', available_actions: 'dict') -> int:
        print("Current State : ")
        print(info_set)

        chosen_action_key = None

        while chosen_action_key is None:
            print("Choose From Available Actions : ")
            print(available_actions)
            try:
                input_action = int(input())
                if input_action in available_actions:
                    chosen_action_key = input_action
            except:
                continue

        return available_actions[chosen_action_key]

    def inform_action_result(self, agent_id: int, reward: 'dict',
                             score: 'dict', terminal: bool):
        print("Player : " + str(agent_id))
        print("Reward : " + str(reward))
        print("Score : " + str(score))
        print("GameOver : " + str(terminal))
