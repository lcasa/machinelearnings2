from random import random

from numpy.random import choice
import random
from machinelearnings2.contracts.agents import Agent
from machinelearnings2.contracts.information_sets import InformationSet


class RandomAgent(Agent):
    def act(self, agent_id: int, info_set: 'InformationSet', available_actions: 'dict') -> int:
        return choice(list(available_actions.values()))

    def inform_action_result(self, agent_id: int, reward: 'dict',
                             score: 'dict', terminal: bool):
        pass


class RandomAgentNonNumpy(Agent):
    def act(self, agent_id: int, info_set: 'InformationSet', available_actions: 'dict') -> int:
        return random.choice(list(available_actions.values()))

    def inform_action_result(self, agent_id: int, reward: 'dict',
                             score: 'dict', terminal: bool):
        pass
