from numpy.core.multiarray import ndarray
from tensorflow import sigmoid
from tensorflow.contrib.layers import relu
from tensorflow.python.keras import Sequential, Input
from tensorflow.python.keras._impl.keras.losses import mse
from tensorflow.python.keras._impl.keras.optimizers import sgd
from tensorflow.python.layers.core import Dense

from machinelearnings2.contracts.agents import Agent
from machinelearnings2.contracts.deep_learning.brains import Brain
from machinelearnings2.contracts.information_sets import InformationSet


class VanillaDeepQLearningBrain(Brain):

    def __init__(self, state_dim: int, max_action_count: int,
                 hidden_layer_count: int, neuron_per_hidden_layer_count: int):
        model = Sequential()
        model.add(Input(shape=(state_dim,)))

        for i in range(hidden_layer_count):
            model.add(Dense(neuron_per_hidden_layer_count,activation=relu))

        model.add(Dense(max_action_count, sigmoid))
        model.compile(loss=mse, optimizer=sgd())
        self.model = model

    def predict(self, input_state: 'ndarray') -> 'ndarray':
        self.model.predict(input_state)

    def train(self, input_state: 'ndarray', expected_target: 'ndarray'):
        self.model.train_on_batch(input_state, expected_target)


class VanillaDeepQLearningAgent(Agent):

    def __init__(self, state_dim: int, max_action_count: int,
                 hidden_layer_count: int = 3, neuron_per_hidden_layer_count: int = 32,
                 gamma: float = 0.99):
        self.Q = VanillaDeepQLearningBrain(state_dim, max_action_count, hidden_layer_count, neuron_per_hidden_layer_count)
        self.gamma = gamma

        self.r = None
        self.a = None
        self.s = None

    def act(self, agent_id: int, info_set: 'InformationSet', available_actions: 'dict') -> int:
        if info_set not in self.Q:
            self.Q[info_set] = dict()
            for action_id in available_actions:
                self.Q[info_set][action_id] = 1.0

        if self.s is not None:
            self.Q[self.s][self.a] += self.alpha * (self.r +
                                                    self.gamma * max(self.Q[info_set].values()) -
                                                    self.Q[self.s][self.a])
            self.r = None
            self.a = None
            self.s = None

        brain_input = info_set.vectorize()
        predicted_Q_values = self.Q.predict([brain_input])[0]

        best_action = None
        best_long_term_reward = None
        for action_id in available_actions:
            if best_action is None or predicted_Q_values[action_id] > best_long_term_reward:
                best_action = action_id
                best_long_term_reward = predicted_Q_values[action_id]

        self.s = info_set
        self.a = best_action
        self.predicted_q_values = predicted_Q_values
        self.r = 0

        return available_actions[best_action]

    def inform_action_result(self, agent_id: int, reward: 'dict', score: 'dict', terminal: bool):
        self.r += reward[agent_id]

        if terminal:
            target = self.predicted_q_values
            self.Q[self.s][self.a] += self.alpha * (self.r - self.Q[self.s][self.a])
            self.r = None
            self.a = None
            self.s = None
