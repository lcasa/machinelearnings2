using System.Collections.Generic;

namespace DRLEnv.Contracts
{
    public interface IAgent<TInformationSet, TValue>
    where TInformationSet : IInformationSet<TInformationSet>
    {
        (TValue actionValue, bool ready) Act(int playerId, TInformationSet informationSet, 
            IReadOnlyDictionary<int, TValue> availableActions);

        void InformActionResult(int playerId,
            IReadOnlyDictionary<int, float> rewards,
            IReadOnlyDictionary<int, float> scores,
            bool terminal);
    }
}