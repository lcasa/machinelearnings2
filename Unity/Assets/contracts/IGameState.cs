﻿using System.Collections.Generic;

namespace DRLEnv.Contracts
{
    public interface IGameState<TGameState, TActionValue>
        where TGameState : IGameState<TGameState, TActionValue>
    {
        int PlayerCount { get; }
        int ActivePlayer { get; }
        bool GameOver { get; }
        IReadOnlyDictionary<int, float> CurrentScores { get; }

        IReadOnlyDictionary<int, TActionValue> GetAvailableActionForPlayer(int playerId);
        void Next(int playerId, TActionValue actionValue);
        void Reset();

        TGameState Clone();
    }
}