namespace DRLEnv.Contracts
{
    public interface IGameStateInformationSetConverter<TGameState, TInformationSet, TActionValue>
        where TGameState : IGameState<TGameState, TActionValue>
        where TInformationSet : IInformationSet<TInformationSet>
    {
        TGameState Determinize(TInformationSet informationSet);
        TInformationSet GetInformationSetForPlayerId(TGameState gameState, int playerId);
    }
}