namespace DRLEnv.Contracts
{
    public interface IInformationSet<TInformationSet>
        where TInformationSet : IInformationSet<TInformationSet>
    {
        // Don't forget to override GetHashCode !
        // Don't forget to Override Equals !
        // Don't forget to Override ToString !
        float[] Vectorize();

        TInformationSet Clone();
    }
}