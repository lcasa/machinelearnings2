using System.Collections.Generic;

namespace DRLEnv.Contracts
{
    public interface IRunner
    {
        IReadOnlyDictionary<int, float> Run(int maxRounds);
    }
}