import numpy as np

from contracts.game_states import GameState
from contracts.information_sets import InformationSet


class BreackOutGameState(GameState):

    def __init__(self):
        self.board = np.zeros((3, 3))
        self.game_over = False
        self.score = 0

    def player_count(self) -> int:
        return 1

    def get_active_player_id(self) -> int:
        return 1

    def get_information_set_for_player(self, player_id: int) -> 'InformationSet':
        from games.breackout.information_sets import BreackOutInformationSet
        return BreackOutInformationSet(self)

    def get_available_actions_for_player(self, player_id: int) -> 'dict':
        raise NotImplementedError()

    def next(self, player_id: int, action_value):
        raise NotImplementedError()

    def is_game_over(self) -> bool:
        raise NotImplementedError()

    def get_current_scores(self) -> 'dict':
        raise NotImplementedError()

    def copy_game_state(self) -> 'GameState':
        raise NotImplementedError()

    def reset(self):
        raise NotImplementedError()