from numpy.core.multiarray import ndarray

from contracts.game_states import GameState
from contracts.information_sets import InformationSet
from games.breackout.game_states import BreackOutGameState


class BreackOutInformationSet(InformationSet):

    def __hash__(self) -> int:
        raise NotImplementedError()

    def __eq__(self, other):
        raise NotImplementedError()

    def __ne__(self, other):
        raise NotImplementedError()

    def determinize(self) -> 'GameState':
        raise NotImplementedError()

    def vectorize(self) -> 'ndarray':
        raise NotImplementedError()

    def __str__(self):
        raise NotImplementedError()

