from machinelearnings2.agents.command_line_agents import CommandLineAgent
from machinelearnings2.games.frogger.game_states import FroggerGameState
from machinelearnings2.runners.basic_runner import BasicRunner

if __name__ == "__main__":
    gameRunner = BasicRunner(FroggerGameState(), [CommandLineAgent()])

    gameRunner.run(5)
