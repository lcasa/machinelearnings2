from enum import Enum

import numpy as np

from machinelearnings2.contracts.game_states import GameState
from machinelearnings2.contracts.information_sets import InformationSet


class Moves(Enum):
    UP = 1
    LEFT = 2
    RIGHT = 3
    DOWN = 4
    NOTHING = 0


class FroggerGameState(GameState):

    def __init__(self):
        self.game_over = False
        self.board = np.zeros((12, 14))
        self.score = {1: 0}
        self.player_position = (11, 6)
        rand_range = 0.65

        for i in range(12):
            for j in range (14):
                suite = True
                if i == 11:
                    self.board[i, j] = 1
                    suite = False
                if i == 6:
                    self.board[i, j] = 1
                    suite = False
                if i == 0:
                    suite = False
                    if j % 3 == 0:
                        self.board[i, j] = 2
                    else:
                        self.board[i, j] = 0
                if suite and self.board[i, j] == 0 and np.random.random() <= rand_range :
                    self.board[i, j] = 1

    def player_count(self) -> int:
        return 1

    def get_active_player_id(self) -> int:
        return 1

    def get_information_set_for_player(self, player_id: int) -> 'InformationSet':
        from machinelearnings2.games.frogger.information_sets import FroggerInformationSet
        return FroggerInformationSet(self)

    def get_available_actions_for_player(self, player_id: int) -> 'dict':
        available_actions = {}
        (pi, pj) = self.player_position
        i = 0
        available_actions[i] = Moves.NOTHING
        i += 1
        if (pi - 1) >= 0:
            available_actions[i] = Moves.UP
            i += 1
        if (pj - 1) >= 0:
            available_actions[i] = Moves.LEFT
            i += 1
        if (pj + 1) <= 13:
            available_actions[i] = Moves.RIGHT
            i += 1
        if (pi + 1) <= 11:
            available_actions[i] = Moves.DOWN

        return available_actions

    def next(self, player_id: int, action_value):
        assert not self.game_over
        assert isinstance(action_value, Moves)
        (pi, pj) = self.player_position

        if action_value == Moves.UP:  # Haut
            action_coords = (pi-1, pj)
        if action_value == Moves.LEFT:  # Gauche
            action_coords = (pi, pj-1)
        if action_value == Moves.RIGHT:  # Droite
            action_coords = (pi, pj+1)
        if action_value == Moves.DOWN:  # Bas
            action_coords = (pi+1, pj)
        if action_value == Moves.NOTHING:  # Rien à faire
            action_coords = self.player_position

        self.player_position = action_coords

        (terminal, win) = self._check_game_over(action_coords)

        self.game_over = terminal
        if win != 0:
            self.score[1] = win

    def _check_game_over(self, action_coords) -> (bool, int):
        (ti, tj) = action_coords
        over = False
        win = 0
        if self.board[action_coords] == 2:
            win = 1
            over = True
        elif self.board[action_coords] == 3:
            win = 3
            over = True

        if self.board[ti, tj] == 0:
            over = True

        return over, win

    def is_game_over(self) -> bool:
        return self.game_over

    def get_current_scores(self) -> 'dict':
        return self.score

    def copy_game_state(self) -> 'FroggerGameState':
        gs = FroggerGameState()
        gs.game_over = self.game_over
        gs.score = self.score
        gs.board = self.board.copy()
        return gs

    def reset(self):
        self.__init__()
