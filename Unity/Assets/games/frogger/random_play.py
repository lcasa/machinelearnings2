from machinelearnings2.agents.random_agents import RandomAgent
from machinelearnings2.games.frogger.game_states import FroggerGameState
from machinelearnings2.runners.basic_runner import BasicRunner

if __name__ == "__main__":
    gameRunner = BasicRunner(FroggerGameState(), [RandomAgent()])

    print(gameRunner.run(10000))
