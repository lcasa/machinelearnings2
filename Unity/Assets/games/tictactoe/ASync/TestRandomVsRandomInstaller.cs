using System.Collections.Generic;
using DRLEnv.Agents;
using DRLEnv.Contracts;
using DRLEnv.Runners;
using Zenject;
using TGameState = DRLEnv.Games.TicTacToe.TicTacToeGameState;
using TInformationSet = DRLEnv.Games.TicTacToe.TicTacToeInformationSet;
using TActionValue = DRLEnv.Games.TicTacToe.TicTacToeAction;
using TConverter = DRLEnv.Games.TicTacToe.TicTacToeGameStateToInformationSetConverter;

namespace DRLEnv.Games.TicTacToe.ASync
{
    public class TestRandomVsRandomInstaller : MonoInstaller<TestRandomVsRandomInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<TGameState>()
                .FromInstance(new TGameState())
                .AsSingle()
                .NonLazy();

            Container.Bind<IEnumerable<IAgent<TInformationSet, TActionValue>>>()
                .FromInstance(new[]
                {
                    new RandomAgent<TInformationSet, TActionValue>(),
                    new RandomAgent<TInformationSet, TActionValue>()
                });

            Container.Bind<TConverter>()
                .FromInstance(new TConverter())
                .AsSingle()
                .NonLazy();

            Container.BindInterfacesAndSelfTo<
                    InfiniteUnityTurnByTurnFrameRunner<
                        TGameState, TInformationSet,
                        TConverter,
                        TActionValue
                    >
                >()
                .AsSingle()
                .NonLazy();
        }
    }
}