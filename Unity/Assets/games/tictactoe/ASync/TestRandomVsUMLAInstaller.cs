using System;
using System.Collections.Generic;
using DRLEnv.Agents;
using DRLEnv.Contracts;
using DRLEnv.Games.TicTacToe.UnityMLAgent;
using DRLEnv.Runners;
using UnityEngine;
using Zenject;
using TGameState = DRLEnv.Games.TicTacToe.TicTacToeGameState;
using TInformationSet = DRLEnv.Games.TicTacToe.TicTacToeInformationSet;
using TActionValue = DRLEnv.Games.TicTacToe.TicTacToeAction;
using TConverter = DRLEnv.Games.TicTacToe.TicTacToeGameStateToInformationSetConverter;
using TUnityMLAgent = DRLEnv.Games.TicTacToe.UnityMLAgent.TicTacToeDRLUnityMLAgent;

namespace DRLEnv.Games.TicTacToe.ASync
{
    public class TestRandomVsUMLAInstaller : MonoInstaller<TestRandomVsUMLAInstaller>
    {
        [SerializeField]
        public TUnityMLAgent uMLA;
        
        public override void InstallBindings()
        {
            Container.Bind<TGameState>()
                .FromInstance(new TGameState())
                .AsSingle()
                .NonLazy();

            Container.Bind<IEnumerable<IAgent<TInformationSet, TActionValue>>>()
                .FromInstance(
                    new IAgent<TInformationSet, TActionValue>[]
                    {
                        new RandomAgent<TInformationSet, TActionValue>(), 
                        uMLA
                    })
                .AsSingle()
                .NonLazy();

            Container.Bind<TConverter>()
                .FromInstance(new TConverter())
                .AsSingle()
                .NonLazy();

            Container.BindInterfacesAndSelfTo<
                    InfiniteUnityTurnByTurnFrameRunner<
                        TGameState, TInformationSet,
                        TConverter,
                        TActionValue
                    >
                >()
                .AsSingle()
                .NonLazy();
        }
    }
}