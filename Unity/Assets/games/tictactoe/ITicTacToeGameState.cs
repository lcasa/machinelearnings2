using DRLEnv.Contracts;

namespace DRLEnv.Games.TicTacToe
{
    public interface ITicTacToeGameState<TGameState> : IGameState<TGameState, TicTacToeAction>
        where TGameState : IGameState<TGameState, TicTacToeAction>
    {
    }
}