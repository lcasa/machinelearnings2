using DRLEnv.Contracts;

namespace DRLEnv.Games.TicTacToe
{
    public interface ITicTacToeInformationSet<TInformationSet> : IInformationSet<TInformationSet>
        where TInformationSet : IInformationSet<TInformationSet>
    {
    }
}