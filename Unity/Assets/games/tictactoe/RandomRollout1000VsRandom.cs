/*from machinelearnings2.agents.random_agents import RandomAgent
from machinelearnings2.agents.random_rollouts_agents import RandomRolloutsAgent
from machinelearnings2.games.tictactoe.game_states import TicTacToeGameState
from machinelearnings2.runners.basic_runner import BasicRunner

if __name__ == "__main__":
    gameRunner = BasicRunner(TicTacToeGameState(), [RandomRolloutsAgent(
        100, [RandomAgent(), RandomAgent()]
    ),
        RandomAgent()])

    print(gameRunner.run(100))*/
using System.Diagnostics;
using System.Collections.Generic;
using DRLEnv.Agents;
using DRLEnv.Contracts;
using DRLEnv.Runners;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace DRLEnv.Games.TicTacToe
{
    public class RandomRollout1000VsRandom : MonoBehaviour
    {
        // Start is called before the first frame update
        private void Start()
        {
            launch(100000);
        }
        public void launch(int roundCount)
        {
            var runner =
                new BasicRunner<TicTacToeGameState, TicTacToeInformationState,
                    TicTacToeGameStateToInformationSetConverter, TicTacToeAction>(new TicTacToeGameState(),
                    new IAgent<TicTacToeInformationState, TicTacToeAction>[] {
                        new RandomAgent<TicTacToeInformationState, TicTacToeAction>(),
                        new RandomRolloutAgent<TicTacToeGameState, TicTacToeInformationState, TicTacToeAction>()
                    });

            //var roundCount = 100000;
            var sw = new Stopwatch();
            sw.Start();
            var results = runner.Run(roundCount);
            sw.Stop();

            Debug.Log($"I did {roundCount} in {sw.ElapsedMilliseconds}ms");
            float total = 0;
            foreach (var kv in results)
            {
                Debug.Log($"Player {kv.Key} scored {kv.Value}");
                total += kv.Value;
            }
            Debug.Log($"Total:  {total}");
        }
    }
}