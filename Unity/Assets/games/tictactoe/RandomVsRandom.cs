using System.Diagnostics;
using DRLEnv.Agents;
using DRLEnv.Runners;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace DRLEnv.Games.TicTacToe
{
    public class RandomVsRandom : MonoBehaviour
    {
        // Start is called before the first frame update
        private void Start()
        {
            launch(100000);
        }
        public void launch(int roundCount)
        {
            var runner =
                new BasicRunner<TicTacToeGameState, TicTacToeInformationState,
                    TicTacToeGameStateToInformationSetConverter, TicTacToeAction>(new TicTacToeGameState(),
                    new[]
                    {
                        new RandomAgent<TicTacToeInformationState, TicTacToeAction>(),
                        new RandomAgent<TicTacToeInformationState, TicTacToeAction>(),
                    });

            //var roundCount = 100000;
            var sw = new Stopwatch();
            sw.Start();
            var results = runner.Run(roundCount);
            sw.Stop();

            Debug.Log($"I did {roundCount} in {sw.ElapsedMilliseconds}ms");
            float total = 0;
            foreach (var kv in results)
            {
                Debug.Log($"Player {kv.Key} scored {kv.Value}");
                total += kv.Value;
            }
            Debug.Log($"Total:  {total}");
        }
    }
}