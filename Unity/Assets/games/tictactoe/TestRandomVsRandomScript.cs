﻿using System.Diagnostics;
using DRLEnv.Agents;
using DRLEnv.Runners;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace DRLEnv.Games.TicTacToe
{
    public class TestRandomVsRandomScript : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            var runner =
                new BasicRunner<TicTacToeGameState, TicTacToeInformationState,
                    TicTacToeGameStateToInformationSetConverter, TicTacToeAction>(new TicTacToeGameState(),
                    new[]
                    {
                        new RandomAgent<TicTacToeInformationState, TicTacToeAction>(),
                        new RandomAgent<TicTacToeInformationState, TicTacToeAction>(),
                    });

            var roundCount = 100000;
            var sw = new Stopwatch();
            sw.Start();
            var results = runner.Run(roundCount);
            sw.Stop();

            Debug.Log($"I did {roundCount} in {sw.ElapsedMilliseconds}ms");
            foreach (var kv in results)
            {
                Debug.Log($"Player {kv.Key} scored {kv.Value}");
            }
        }
    }
}