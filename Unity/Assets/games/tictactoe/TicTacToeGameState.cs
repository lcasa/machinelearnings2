using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DRLEnv.Games.TicTacToe
{
    public class TicTacToeGameState : ITicTacToeGameState<TicTacToeGameState>, IReadOnlyDictionary<int, float>
    {
        public int ActivePlayer { get; private set; }
        public bool GameOver { get; private set; }

        public IReadOnlyDictionary<int, float> CurrentScores => this;

        public int PlayerCount => 2;

        private readonly Dictionary<int, TicTacToeAction> actionForPlayer = new Dictionary<int, TicTacToeAction>();
        public int[] Board { get; }

        public float Player1Score { get; private set; }
        public float Player2Score { get; private set; }

        public TicTacToeGameState()
        {
            Board = new[] {0, 0, 0, 0, 0, 0, 0, 0, 0};
            Reset();
        }

        public TicTacToeGameState(int activePlayer,
            bool gameOver,
            int[] board,
            float player1Score,
            float player2Score)
        {
            ActivePlayer = activePlayer;
            GameOver = gameOver;
            Board = board;
            Player1Score = player1Score;
            Player2Score = player2Score;
        }

        public IReadOnlyDictionary<int, TicTacToeAction> GetAvailableActionForPlayer(int playerId)
        {
            if (playerId != ActivePlayer)
            {
                throw new Exception(
                    $"Player Id {playerId} is not the active player ! He has no action available right now");
            }

            actionForPlayer.Clear();
            for (var i = 0; i < Board.Length; i++)
            {
                if (Board[i] == 0)
                {
                    actionForPlayer.Add(i, (TicTacToeAction) i);
                }
            }

            return actionForPlayer;
        }

        public void Next(int playerId, TicTacToeAction actionValue)
        {
            if (GameOver)
            {
                throw new Exception("Game is over, no point to call 'next' on it !");
            }

            if (playerId != ActivePlayer)
            {
                throw new Exception($"Player Id {playerId} is not the active player ! He can't play right now !");
            }

            var actionCoord = (int) actionValue;
            if ((int) actionValue < 0 || 8 < (int) actionValue)
            {
                throw new Exception("Invalid Action !");
            }

            Board[actionCoord] = ActivePlayer;

            var (winner, terminal) = CheckGameOver(actionCoord);

            GameOver = terminal;
            var otherPlayer = OtherPlayer();

            if (winner != 0)
            {
                Player1Score = ActivePlayer == 1 ? 1.0f : 0.0f;
                Player2Score = ActivePlayer == 1 ? 0.0f : 1.0f;
            }
            else if (terminal)
            {
                Player1Score = 0.5f;
                Player2Score = 0.5f;
            }

            ActivePlayer = otherPlayer;
        }

        private int OtherPlayer()
        {
            return ActivePlayer % 2 + 1;
        }

        private (int, bool) CheckGameOver(int actionCoord)
        {
            var (ti, tj) = (actionCoord / 3, actionCoord % 3);

            if (Board[ti * 3 + 0] == Board[ti * 3 + 1] && Board[ti * 3 + 1] == Board[ti * 3 + 2])
                return (ActivePlayer, true);

            if (Board[0 * 3 + tj] == Board[1 * 3 + tj] && Board[1 * 3 + tj] == Board[2 * 3 + tj])
                return (ActivePlayer, true);

            if (Board[0 * 3 + 0] == Board[1 * 3 + 1]
                && Board[1 * 3 + 1] == Board[2 * 3 + 2]
                && Board[0 * 3 + 0] == ActivePlayer)
                return (ActivePlayer, true);

            if (Board[2 * 3 + 0] == Board[1 * 3 + 1]
                && Board[1 * 3 + 1] == Board[0 * 3 + 2]
                && Board[2 * 3 + 0] == ActivePlayer)
                return (ActivePlayer, true);

            return Board.Any(t => t == 0) ? (0, false) : (0, true);
        }

        public void Reset()
        {
            ActivePlayer = 1;
            GameOver = false;
            Player1Score = 0.0f;
            Player2Score = 0.0f;
            actionForPlayer.Clear();
            for (var i = 0; i < Board.Length; i++)
            {
                Board[i] = 0;
            }
        }

        public TicTacToeGameState Clone()
        {
            var board = new int[Board.Length];
            Array.Copy(Board, board, board.Length);

            var gs = new TicTacToeGameState(ActivePlayer,
                GameOver,
                board,
                Player1Score,
                Player2Score);

            return gs;
        }

        public IEnumerator<KeyValuePair<int, float>> GetEnumerator()
        {
            yield return new KeyValuePair<int, float>(1, Player1Score);
            yield return new KeyValuePair<int, float>(2, Player2Score);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public int Count => 2;

        public bool ContainsKey(int key)
        {
            return key == 1 || key == 2;
        }

        public bool TryGetValue(int key, out float value)
        {
            switch (key)
            {
                case 1:
                    value = Player1Score;
                    return true;
                case 2:
                    value = Player2Score;
                    return true;
                default:
                    value = 0;
                    return false;
            }
        }

        public float this[int key]
        {
            get
            {
                switch (key)
                {
                    case 1:
                        return Player1Score;
                    case 2:
                        return Player2Score;
                    default:
                        throw new KeyNotFoundException();
                }
            }
        }

        public IEnumerable<int> Keys
        {
            get
            {
                yield return 1;
                yield return 2;
            }
        }

        public IEnumerable<float> Values
        {
            get
            {
                yield return Player1Score;
                yield return Player2Score;
            }
        }
    }
}