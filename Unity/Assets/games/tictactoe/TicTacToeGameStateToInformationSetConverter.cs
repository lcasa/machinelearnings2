using DRLEnv.Contracts;

namespace DRLEnv.Games.TicTacToe
{
    public struct TicTacToeGameStateToInformationSetConverter : IGameStateInformationSetConverter<
        TicTacToeGameState, TicTacToeInformationSet, TicTacToeAction>
    {
        public TicTacToeGameState Determinize(TicTacToeInformationSet informationSet)
        {
            var board = new int[informationSet.Board.Length];

            for (var i = 0; i < board.Length; i++)
            {
                board[i] = informationSet.Board[i];
            }

            var gs = new TicTacToeGameState(informationSet.ActivePlayer,
                informationSet.GameOver,
                informationSet.Board,
                informationSet.Player1Score,
                informationSet.Player2Score);

            return gs;
        }

        public TicTacToeInformationSet GetInformationSetForPlayerId(TicTacToeGameState gameState, int playerId)
        {
            var infoSet = new TicTacToeInformationSet(gameState.ActivePlayer,
                gameState.GameOver,
                gameState.Board,
                gameState.Player1Score,
                gameState.Player2Score);

            return infoSet;
        }
    }
}