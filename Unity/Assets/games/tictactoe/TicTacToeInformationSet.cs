using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DRLEnv.Games.TicTacToe
{
    public class TicTacToeInformationSet : ITicTacToeInformationSet<TicTacToeInformationSet>,
        IReadOnlyDictionary<int, float>
    {
        public int ActivePlayer { get; }
        public bool GameOver { get; }
        public int[] Board { get; }
        public float Player1Score { get; }
        public float Player2Score { get; }

        private float[] boardVectorized;

        private string cachedToString;

        private static readonly char[] putToString =
        {
            '_', 'X', 'O'
        };

        public TicTacToeInformationSet(int activePlayer,
            bool gameOver,
            int[] board,
            float player1Score,
            float player2Score)
        {
            ActivePlayer = activePlayer;
            GameOver = gameOver;
            Board = board;
            Player1Score = player1Score;
            Player2Score = player2Score;
        }

        public float[] Vectorize()
        {
            if (boardVectorized != null)
            {
                return boardVectorized;
            }

            boardVectorized = new float[Board.Length * 3];
            for (var i = 0; i < Board.Length; i++)
            {
                boardVectorized[i + Board[i]] = 1.0f;
            }

            return boardVectorized;
        }

        public TicTacToeInformationSet Clone()
        {
            var infoSet = new TicTacToeInformationSet(ActivePlayer,
                GameOver,
                Board,
                Player1Score,
                Player2Score);

            return infoSet;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            if (cachedToString == null)
            {
                sb.AppendLine($"Active Player : {ActivePlayer} ");
                for (var i = 0; i < 3; i++)
                {
                    for (var j = 0; j < 3; j++)
                    {
                        sb.Append(putToString[Board[i * 3 + j]]);
                    }

                    sb.AppendLine();
                }

                cachedToString = sb.ToString();
            }

            return cachedToString;
        }

        public override bool Equals(object obj)
        {
            var other = (TicTacToeInformationSet) obj;

            return other != null && Board.SequenceEqual(other.Board);
        }

        public override int GetHashCode()
        {
            var hash = 0;

            for (var i = 0; i < Board.Length; i++)
            {
                var val = Board[i];

                if (val == 0)
                {
                    continue;
                }

                var pow = 1;
                for (var j = 0; j < i; j++)
                {
                    pow *= 3;
                }

                hash += pow * val;
            }

            return hash;
        }


        public IEnumerator<KeyValuePair<int, float>> GetEnumerator()
        {
            yield return new KeyValuePair<int, float>(1, Player1Score);
            yield return new KeyValuePair<int, float>(1, Player2Score);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public int Count => 2;

        public bool ContainsKey(int key)
        {
            return key == 1 || key == 2;
        }

        public bool TryGetValue(int key, out float value)
        {
            switch (key)
            {
                case 1:
                    value = Player1Score;
                    return true;
                case 2:
                    value = Player2Score;
                    return true;
                default:
                    value = 0;
                    return false;
            }
        }

        public float this[int key]
        {
            get
            {
                switch (key)
                {
                    case 1:
                        return Player1Score;
                    case 2:
                        return Player2Score;
                    default:
                        throw new KeyNotFoundException();
                }
            }
        }

        public IEnumerable<int> Keys
        {
            get
            {
                yield return 1;
                yield return 2;
            }
        }

        public IEnumerable<float> Values
        {
            get
            {
                yield return Player1Score;
                yield return Player2Score;
            }
        }
    }
}