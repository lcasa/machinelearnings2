from machinelearnings2.agents.random_agents import RandomAgent
from machinelearnings2.agents.random_rollouts_agents import RandomRolloutsAgent
from machinelearnings2.agents.tabular_q_learning_agents import TabularQLearningAgent
from machinelearnings2.games.tictactoe.game_states import TicTacToeGameState
from machinelearnings2.runners.basic_runner import BasicRunner

if __name__ == "__main__":
    gameRunner = BasicRunner(TicTacToeGameState(), [
        TabularQLearningAgent(),
        RandomAgent()])

    for i in range(1000):
        print(gameRunner.run(10000))
