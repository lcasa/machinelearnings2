using System.Collections.Generic;
using System.Threading;
using DRLEnv.Contracts;

namespace DRLEnv.Runners
{
    public class BasicRunner<TGameState, TInformationSet, TConverter, TActionValue> : IRunner
        where TGameState : IGameState<TGameState, TActionValue>
        where TInformationSet : IInformationSet<TInformationSet>
        where TConverter : IGameStateInformationSetConverter<TGameState, TInformationSet, TActionValue>, new()
    {
        private readonly TGameState initialGameState;
        private readonly IEnumerable<IAgent<TInformationSet, TActionValue>> availableAgents;
        private readonly TConverter converter;
        private readonly bool reset;
        private readonly bool copy;

        public BasicRunner(
            TGameState initialGameState,
            IEnumerable<IAgent<TInformationSet, TActionValue>> availableAgents,
            bool reset = true,
            bool copy = false)
        {
            this.initialGameState = initialGameState;
            this.reset = reset;
            this.availableAgents = availableAgents;
            this.copy = copy;
            converter = new TConverter();
        }

        public IReadOnlyDictionary<int, float> Run(int maxRounds)
        {
            var accumulatedScores = new Dictionary<int, float>();
            var gs = initialGameState;

            var playingAgents = new Dictionary<int, IAgent<TInformationSet, TActionValue>>();
            var idleAgents = new Queue<IAgent<TInformationSet, TActionValue>>();
            var rewards = new Dictionary<int, float>();
            for (var i = 0; i < maxRounds; i++)
            {
                if (copy)
                {
                    gs = initialGameState.Clone();
                }

                if (reset)
                {
                    gs.Reset();
                }

                playingAgents.Clear();
                foreach (var agent in availableAgents)
                {
                    idleAgents.Enqueue(agent);
                }

                var currentScores = gs.CurrentScores;

                while (!gs.GameOver)
                {
                    var activePlayer = gs.ActivePlayer;
                    rewards.Clear();
                    foreach (var kv in gs.CurrentScores)
                    {
                        rewards.Add(kv.Key, kv.Value);
                    }

                    if (!playingAgents.ContainsKey(activePlayer))
                    {
                        playingAgents.Add(activePlayer, idleAgents.Dequeue());
                    }

                    var availableActions = gs.GetAvailableActionForPlayer(activePlayer);

                    TActionValue actionValue;
                    while (true)
                    {
                        bool ready;
                        (actionValue, ready) = playingAgents[activePlayer]
                            .Act(activePlayer,
                                converter.GetInformationSetForPlayerId(gs, activePlayer),
                                availableActions);

                        if (ready)
                        {
                            break;
                        }

                        Thread.Sleep(10);
                    }

                    gs.Next(activePlayer, actionValue);

                    var terminal = gs.GameOver;

                    foreach (var kv in currentScores)
                    {
                        if (rewards.ContainsKey(kv.Key))
                        {
                            rewards[kv.Key] = kv.Value - rewards[kv.Key];
                        }
                        else
                        {
                            rewards.Add(kv.Key, kv.Value);
                        }
                    }

                    foreach (var kv in playingAgents)
                    {
                        kv.Value.InformActionResult(kv.Key, rewards, currentScores, terminal);
                    }
                }

                foreach (var kv in playingAgents)
                {
                    if (accumulatedScores.ContainsKey(kv.Key))
                    {
                        accumulatedScores[kv.Key] += currentScores[kv.Key];
                    }
                    else
                    {
                        accumulatedScores.Add(kv.Key, currentScores[kv.Key]);
                    }
                }
            }

            return accumulatedScores;
        }
    }
}