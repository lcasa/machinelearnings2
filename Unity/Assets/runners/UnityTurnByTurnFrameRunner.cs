using System;
using System.Collections.Generic;
using DRLEnv.Contracts;
using UniRx;
using Zenject;

namespace DRLEnv.Runners
{
    public class
        InfiniteUnityTurnByTurnFrameRunner<TGameState, TInformationSet, TConverter, TActionValue> : IInitializable,
            IDisposable
        where TGameState : IGameState<TGameState, TActionValue>
        where TInformationSet : IInformationSet<TInformationSet>
        where TConverter : IGameStateInformationSetConverter<TGameState, TInformationSet, TActionValue>, new()
    {
        [Inject]
        private TGameState initialGameState;

        [Inject]
        private IEnumerable<IAgent<TInformationSet, TActionValue>> availableAgents;

        [Inject]
        private TConverter converter;

        private TGameState currentGameState;

        private readonly Dictionary<int, float> accumulatedScores = new Dictionary<int, float>();

        private readonly Dictionary<int, IAgent<TInformationSet, TActionValue>> playingAgents =
            new Dictionary<int, IAgent<TInformationSet, TActionValue>>();

        private readonly Queue<IAgent<TInformationSet, TActionValue>> idleAgents =
            new Queue<IAgent<TInformationSet, TActionValue>>();

        private int currentRound;

        private readonly Dictionary<int, float> rewards = new Dictionary<int, float>();

        private readonly CompositeDisposable disposable = new CompositeDisposable();

        private void RunFrame()
        {
            if (currentGameState.GameOver)
            {
                Reset();
            }

            var activePlayer = currentGameState.ActivePlayer;
            rewards.Clear();
            foreach (var kv in currentGameState.CurrentScores)
            {
                rewards.Add(kv.Key, kv.Value);
            }

            if (!playingAgents.ContainsKey(activePlayer))
            {
                playingAgents.Add(activePlayer, idleAgents.Dequeue());
            }

            var availableActions = currentGameState.GetAvailableActionForPlayer(activePlayer);

            var (actionValue, ready) = playingAgents[activePlayer]
                .Act(activePlayer,
                    converter.GetInformationSetForPlayerId(currentGameState, activePlayer),
                    availableActions);

            if (!ready)
            {
                return;
            }
            
            var currentScores = currentGameState.CurrentScores;

            currentGameState.Next(activePlayer, actionValue);

            var terminal = currentGameState.GameOver;

            foreach (var kv in currentScores)
            {
                if (rewards.ContainsKey(kv.Key))
                {
                    rewards[kv.Key] = kv.Value - rewards[kv.Key];
                }
                else
                {
                    rewards.Add(kv.Key, kv.Value);
                }
            }

            foreach (var kv in playingAgents)
            {
                kv.Value.InformActionResult(kv.Key, rewards, currentScores, terminal);

                if (terminal)
                {
                    if (accumulatedScores.ContainsKey(kv.Key))
                    {
                        accumulatedScores[kv.Key] += currentScores[kv.Key];
                    }
                    else
                    {
                        accumulatedScores.Add(kv.Key, currentScores[kv.Key]);
                    }
                }
            }
        }

        public void Initialize()
        {
            Reset();
            Observable.EveryUpdate()
                .Subscribe(_ => RunFrame())
                .AddTo(disposable);
        }

        private void Reset()
        {
            currentGameState = initialGameState;
            currentGameState.Reset();
            playingAgents.Clear();

            foreach (var agent in availableAgents)
            {
                idleAgents.Enqueue(agent);
            }
        }

        public void Dispose()
        {
            disposable?.Dispose();
        }
    }
}