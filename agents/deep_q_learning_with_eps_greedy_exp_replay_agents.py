from collections import deque
from random import random, choice, shuffle

import numpy as np

from machinelearnings2.agents.vanilla_deep_q_learning_agents import VanillaDeepQLearningBrain
from machinelearnings2.contracts.agents import Agent
from machinelearnings2.contracts.information_sets import InformationSet


class DeepQLearningWithEpsGreedyExpReplayAgent(Agent):

    def __init__(self, state_dim: int, max_action_count: int,
                 hidden_layer_count: int = 5, neuron_per_hidden_layer_count: int = 64,
                 gamma: float = 0.99, epsilon=0.1, epsilon_geometric_decay=0.999999,
                 buffer_size: int = 4096, batch_size=128,
                 ):
        self.Q = VanillaDeepQLearningBrain(
            state_dim, max_action_count, hidden_layer_count, neuron_per_hidden_layer_count
        )
        self.gamma = gamma

        self.epsilon = epsilon
        self.epsilon_geometric_decay = epsilon_geometric_decay
        self.r = None
        self.a = None
        self.s = None
        self.batch_size = batch_size
        self.memory = deque(maxlen=buffer_size)

    def act(self, agent_id: int, info_set: 'InformationSet', available_actions: 'dict') -> int:

        brain_input = info_set.vectorize()

        if self.s is not None:
            self.memory.append({
                's': self.s,
                'a': self.a,
                'r': self.r,
                'ns': brain_input,
                'naa': list(available_actions.keys())
            })

            self.train()

            self.r = None
            self.a = None
            self.s = None

        predicted_Q_values = self.Q.predict(np.array([brain_input]))[0]

        rdm = random()

        if rdm < self.epsilon:
            best_action = choice(list(available_actions.keys()))
        else:
            best_action = None
            best_long_term_reward = None
            for action_id in available_actions:
                if best_action is None or predicted_Q_values[action_id] > best_long_term_reward:
                    best_action = action_id
                    best_long_term_reward = predicted_Q_values[action_id]

        self.s = brain_input
        self.a = best_action
        self.predicted_q_values = predicted_Q_values
        self.r = 0

        return available_actions[best_action]

    def inform_action_result(self, agent_id: int, reward: 'dict', score: 'dict', terminal: bool):
        self.r += reward[agent_id]

        if terminal:
            self.memory.append({
                's': self.s,
                'a': self.a,
                'r': self.r,
                'ns': None,
                'naa': None
            })

            self.train()

            self.r = None
            self.a = None
            self.s = None

    def train(self):
        if len(self.memory) != self.memory.maxlen:
            return

        shuffle(self.memory)
        batch = list(self.memory)[:self.batch_size]
        states = np.array([batch[i]['s'] for i in range(self.batch_size)])
        next_states = np.array([batch[i]['ns'] for i in range(self.batch_size)])
        rewards = np.array([batch[i]['r'] for i in range(self.batch_size)])
        actions = np.array([batch[i]['a'] for i in range(self.batch_size)])

        target_values = self.Q.predict(states)

        for i in range(self.batch_size):
            if next_states[i] is None:
                target_values[i][actions[i]] = rewards[i]
            else:
                next_state_prediction = self.Q.predict(np.array([next_states[i]]))[0]
                maximum_value = np.max(next_state_prediction[batch[i]['naa']])
                target_values[i][actions[i]] = rewards[i] + self.gamma * maximum_value

        self.Q.train(states, target_values)

        self.epsilon = self.epsilon * self.epsilon_geometric_decay

