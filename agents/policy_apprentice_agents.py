from collections import deque

from numpy.core.multiarray import ndarray
from tensorflow.python.keras import Sequential
from tensorflow.python.keras._impl.keras.activations import relu, softmax
from tensorflow.python.keras._impl.keras.layers import Dense, np
from tensorflow.python.keras._impl.keras.losses import categorical_crossentropy
from tensorflow.python.keras._impl.keras.metrics import categorical_accuracy
from tensorflow.python.keras._impl.keras.models import load_model
from tensorflow.python.keras._impl.keras.optimizers import adam
from tensorflow.python.keras._impl.keras.utils import to_categorical

from machinelearnings2.contracts.agents import Agent
from machinelearnings2.contracts.deep_learning.brains import Brain
from machinelearnings2.contracts.information_sets import InformationSet


class PolicyBrain(Brain):

    def __init__(self, state_dim: int, max_action_count: int,
                 hidden_layer_count: int, neuron_per_hidden_layer_count: int):
        model = Sequential()

        for i in range(hidden_layer_count):
            if i == 0:
                model.add(Dense(neuron_per_hidden_layer_count, input_shape=(state_dim,),
                                activation=relu))
            else:
                model.add(Dense(neuron_per_hidden_layer_count, activation=relu))

        model.add(Dense(max_action_count, softmax))
        model.compile(loss=categorical_crossentropy, optimizer=adam(), metrics=[categorical_accuracy])

        self.model = model

    def predict(self, input_state: 'ndarray') -> 'ndarray':
        return self.model.predict(input_state)

    def train(self, input_state: 'ndarray', expected_target: 'ndarray'):
        self.model.train_on_batch(input_state, expected_target)

    def fit(self, input_state: 'ndarray', expected_target: 'ndarray'):
        self.model.fit(input_state, expected_target, epochs=10, batch_size=32)


class PolicyApprenticeAgent(Agent):

    def __init__(self, expert_agent: Agent,
                 state_dim: int, actions_dim: int, dataset_length: int = 8192, warmup_time: int = 0,
                 num_hidden_layers: int = 5, num_neurons_per_layer: int = 64):
        self.warmup_time = warmup_time
        self.dataset_length = dataset_length
        self.expert = expert_agent
        self.dataset = deque(maxlen=dataset_length)
        self.transition_counter = -1
        self.actions_dim = actions_dim
        self.brain = PolicyBrain(state_dim, actions_dim, num_hidden_layers, num_neurons_per_layer)

    def act(self, agent_id: int, info_set: 'InformationSet', available_actions: 'dict') -> int:

        self.transition_counter += 1

        if self.transition_counter < self.warmup_time:
            return self.expert.act(agent_id, info_set, available_actions)

        if self.transition_counter < self.dataset_length + self.warmup_time:
            action_id = self.expert.act(agent_id, info_set, available_actions)
            self.dataset.append({'s': info_set.vectorize(), 'a': action_id})
            return action_id

        if self.transition_counter == self.dataset_length + self.warmup_time:
            self.train()
            # self.brain.model.save('supermodele.toto')
            # self.brain.model = load_model('supermodele.toto')

        actions_probabilities = self.brain.predict(np.array([info_set.vectorize()]))[0]

        best = np.argmax(actions_probabilities[list(available_actions.keys())])

        return list(available_actions.values())[best]


    def train(self):
        states = np.array([elt['s'] for elt in self.dataset])
        actions = np.array([elt['a'].value for elt in self.dataset])
        actions = to_categorical(actions, self.actions_dim)

        self.brain.fit(states, actions)

    def inform_action_result(self, agent_id: int, reward: 'dict', score: 'dict', terminal: bool):
        if self.transition_counter < self.dataset_length + self.warmup_time:
            self.expert.inform_action_result(agent_id, reward, score, terminal)
