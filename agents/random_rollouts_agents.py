from typing import Iterable

from machinelearnings2.contracts.agents import Agent
from machinelearnings2.contracts.information_sets import InformationSet
from machinelearnings2.runners.basic_runner import BasicRunner


class RandomRolloutsAgent(Agent):

    def __init__(self, rollout_count_per_action: int, agents: 'Iterable[Agent]'):
        self.rollout_count_per_action = rollout_count_per_action
        self.rollout_agents = agents

    def act(self, agent_id: int, info_set: 'InformationSet', available_actions: 'dict') -> int:
        scores = {}

        for action_id in available_actions.keys():
            for i in range(self.rollout_count_per_action):
                gs = info_set.determinize()
                old_score = gs.get_current_scores().copy()
                gs.next(agent_id, available_actions[action_id])
                new_score = gs.get_current_scores()

                reward = {}
                for player in new_score:
                    reward[player] = new_score[player] - (0 if player not in old_score else
                                                          old_score[player])

                runner = BasicRunner(gs, self.rollout_agents, reset=False)
                action_score = runner.run(1)

                for player in reward:
                    action_score[player] = reward[player] + (0 if player not in action_score else
                                                             action_score[player])

                if action_id not in scores:
                    scores[action_id] = {}
                for player_key in action_score:
                    if player_key not in scores[action_id]:
                        scores[action_id][player_key] = 0

                    scores[action_id][player_key] += action_score[player_key]

        # print(info_set)
        best_action = None
        best_score = None
        for action in scores:
            if best_action is None or best_score < scores[action][agent_id]:
                best_score = scores[action][agent_id]
                best_action = action

        return available_actions[best_action]

    def inform_action_result(self, agent_id: int, reward: 'dict', score: 'dict', terminal: bool):
        pass


class RandomRolloutsOptimistAgent(Agent):

    def __init__(self, rollout_count_per_action: int, agents: 'Iterable[Agent]'):
        self.rollout_count_per_action = rollout_count_per_action
        self.rollout_agents = agents

    def act(self, agent_id: int, info_set: 'InformationSet', available_actions: 'dict') -> int:
        scores = {}

        for action_id in available_actions.keys():
            for i in range(self.rollout_count_per_action):
                gs = info_set.determinize()
                # gs.simulation = True
                old_score = gs.get_current_scores().copy()
                gs.next(agent_id, available_actions[action_id])
                new_score = gs.get_current_scores()

                reward = {}
                for player in new_score:
                    reward[player] = new_score[player] - (0 if player not in old_score else
                                                          old_score[player])

                runner = BasicRunner(gs, self.rollout_agents, reset=False)
                action_score = runner.run(1)

                for player in reward:
                    action_score[player] = reward[player] + (0 if player not in action_score else
                                                             action_score[player])

                if action_id not in scores:
                    scores[action_id] = {}
                for player_key in action_score:
                    if player_key not in scores[action_id]:
                        scores[action_id][player_key] = 0

                    scores[action_id][player_key] = max(action_score[player_key], scores[action_id][player_key])

        # print(info_set)
        best_action = None
        best_score = None
        for action in scores:
            if best_action is None or best_score < scores[action][agent_id]:
                best_score = scores[action][agent_id]
                best_action = action

        return available_actions[best_action]

    def inform_action_result(self, agent_id: int, reward: 'dict', score: 'dict', terminal: bool):
        pass
