from random import choice, random

from machinelearnings2.contracts.agents import Agent
from machinelearnings2.contracts.information_sets import InformationSet


class TabularQLearningAgentWithEpsilonGreedyPolicy(Agent):

    def __init__(self, alpha: float = 0.1, gamma: float = 0.99, epsilon=0.1, epsilon_geometric_decay=0.999999
                 ):
        self.Q = dict()
        self.alpha = alpha
        self.gamma = gamma
        self.epsilon = epsilon
        self.epsilon_geometric_decay = epsilon_geometric_decay

        self.r = None
        self.a = None
        self.s = None

    def act(self, agent_id: int, info_set: 'InformationSet', available_actions: 'dict') -> int:
        if info_set not in self.Q:
            self.Q[info_set] = dict()
            for action_id in available_actions:
                self.Q[info_set][action_id] = 0.5

        if self.s is not None:
            self.Q[self.s][self.a] += self.alpha * (self.r +
                                                    self.gamma * max(self.Q[info_set].values()) -
                                                    self.Q[self.s][self.a])
            self.r = None
            self.a = None
            self.s = None

        rdm = random()

        if rdm < self.epsilon:
            best_action = choice(list(available_actions.keys()))
        else:
            best_action = None
            best_long_term_reward = None
            for action_id in self.Q[info_set]:
                if best_action is None or self.Q[info_set][action_id] > best_long_term_reward:
                    best_action = action_id
                    best_long_term_reward = self.Q[info_set][action_id]

        self.s = info_set
        self.a = best_action
        self.r = 0

        self.epsilon *= self.epsilon_geometric_decay

        return available_actions[best_action]

    def inform_action_result(self, agent_id: int, reward: 'dict', score: 'dict', terminal: bool):
        self.r += reward[agent_id]

        if terminal:
            self.Q[self.s][self.a] += self.alpha * (self.r - self.Q[self.s][self.a])
            self.r = None
            self.a = None
            self.s = None
