import numpy as np

from numpy.core.multiarray import ndarray
from tensorflow.python.keras import Sequential
from tensorflow.python.keras._impl.keras.activations import relu, linear
from tensorflow.python.keras._impl.keras.losses import mse
from tensorflow.python.keras._impl.keras.optimizers import adam
from tensorflow.python.layers.core import Dense

from machinelearnings2.contracts.agents import Agent
from machinelearnings2.contracts.deep_learning.brains import Brain
from machinelearnings2.contracts.information_sets import InformationSet


class VanillaDeepQLearningBrain(Brain):

    def __init__(self, state_dim: int, max_action_count: int,
                 hidden_layer_count: int, neuron_per_hidden_layer_count: int):
        model = Sequential()

        for i in range(hidden_layer_count):
            if i == 0:
                model.add(Dense(neuron_per_hidden_layer_count, input_shape=(state_dim,),
                                activation=relu))
            else:
                model.add(Dense(neuron_per_hidden_layer_count, activation=relu))

        model.add(Dense(max_action_count, linear))
        model.compile(loss=mse, optimizer=adam())

        self.model = model

    def predict(self, input_state: 'ndarray') -> 'ndarray':
        return self.model.predict(input_state)

    def train(self, input_state: 'ndarray', expected_target: 'ndarray'):
        self.model.train_on_batch(input_state, expected_target)


class VanillaDeepQLearningAgent(Agent):

    def __init__(self, state_dim: int, max_action_count: int,
                 hidden_layer_count: int = 5, neuron_per_hidden_layer_count: int = 64,
                 gamma: float = 0.99,
                 ):
        self.Q = VanillaDeepQLearningBrain(
            state_dim, max_action_count, hidden_layer_count, neuron_per_hidden_layer_count
        )
        self.gamma = gamma

        self.r = None
        self.a = None
        self.s = None

    def act(self, agent_id: int, info_set: 'InformationSet', available_actions: 'dict') -> int:

        brain_input = info_set.vectorize()
        predicted_Q_values = self.Q.predict(np.array([brain_input]))[0]

        best_action = None
        best_long_term_reward = None
        for action_id in available_actions:
            if best_action is None or predicted_Q_values[action_id] > best_long_term_reward:
                best_action = action_id
                best_long_term_reward = predicted_Q_values[action_id]

        if self.s is not None:
            target = self.predicted_q_values
            target[self.a] = self.r + self.gamma * best_long_term_reward
            self.Q.train(np.array([self.s]), np.array([target]))
            self.r = None
            self.a = None
            self.s = None

        predicted_Q_values = self.Q.predict(np.array([brain_input]))[0]

        best_action = None
        best_long_term_reward = None
        for action_id in available_actions:
            if best_action is None or predicted_Q_values[action_id] > best_long_term_reward:
                best_action = action_id
                best_long_term_reward = predicted_Q_values[action_id]

        self.s = brain_input
        self.a = best_action
        self.predicted_q_values = predicted_Q_values
        self.r = 0

        return available_actions[best_action]

    def inform_action_result(self, agent_id: int, reward: 'dict', score: 'dict', terminal: bool):
        self.r += reward[agent_id]

        if terminal:
            target = self.predicted_q_values
            target[self.a] = self.r

            self.Q.train(np.array([self.s]), np.array([target]))

            self.r = None
            self.a = None
            self.s = None
