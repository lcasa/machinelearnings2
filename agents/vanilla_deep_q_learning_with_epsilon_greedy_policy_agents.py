from random import random, choice

import numpy as np

from machinelearnings2.agents.vanilla_deep_q_learning_agents import VanillaDeepQLearningBrain
from machinelearnings2.contracts.agents import Agent
from machinelearnings2.contracts.information_sets import InformationSet


class VanillaDeepQLearningWithEpsilonGreedyPolicyAgent(Agent):

    def __init__(self, state_dim: int, max_action_count: int,
                 hidden_layer_count: int = 5, neuron_per_hidden_layer_count: int = 64,
                 gamma: float = 0.99, epsilon=0.1, epsilon_geometric_decay=0.999999
                 ):
        self.Q = VanillaDeepQLearningBrain(
            state_dim, max_action_count, hidden_layer_count, neuron_per_hidden_layer_count
        )
        self.gamma = gamma

        self.epsilon = epsilon
        self.epsilon_geometric_decay = epsilon_geometric_decay
        self.r = None
        self.a = None
        self.s = None

    def act(self, agent_id: int, info_set: 'InformationSet', available_actions: 'dict') -> int:

        brain_input = info_set.vectorize()
        predicted_Q_values = self.Q.predict(np.array([brain_input]))[0]

        best_action = None
        best_long_term_reward = None
        for action_id in available_actions:
            if best_action is None or predicted_Q_values[action_id] > best_long_term_reward:
                best_action = action_id
                best_long_term_reward = predicted_Q_values[action_id]

        if self.s is not None:
            target = self.predicted_q_values
            target[self.a] = self.r + self.gamma * best_long_term_reward
            self.Q.train(np.array([self.s]), np.array([target]))
            self.r = None
            self.a = None
            self.s = None

        predicted_Q_values = self.Q.predict(np.array([brain_input]))[0]

        rdm = random()

        if rdm < self.epsilon:
            best_action = choice(list(available_actions.keys()))
        else:
            best_action = None
            best_long_term_reward = None
            for action_id in available_actions:
                if best_action is None or predicted_Q_values[action_id] > best_long_term_reward:
                    best_action = action_id
                    best_long_term_reward = predicted_Q_values[action_id]

        self.epsilon = self.epsilon * self.epsilon_geometric_decay

        self.s = brain_input
        self.a = best_action
        self.predicted_q_values = predicted_Q_values
        self.r = 0

        return available_actions[best_action]

    def inform_action_result(self, agent_id: int, reward: 'dict', score: 'dict', terminal: bool):
        self.r += reward[agent_id]

        if terminal:
            target = self.predicted_q_values
            target[self.a] = self.r

            self.Q.train(np.array([self.s]), np.array([target]))

            self.r = None
            self.a = None
            self.s = None
