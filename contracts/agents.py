from machinelearnings2.contracts.information_sets import InformationSet


class Agent:

    def act(self, agent_id: int, info_set: 'InformationSet',
            available_actions: 'dict') -> int:
        raise NotImplementedError()

    def inform_action_result(self, agent_id: int, reward: 'dict',
                             score: 'dict', terminal: bool):
        raise NotImplementedError()
