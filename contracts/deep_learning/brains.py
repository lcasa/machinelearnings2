from numpy.core.multiarray import ndarray


class Brain:

    def predict(self, input_state: 'ndarray') -> 'ndarray':
        raise NotImplementedError()

    def train(self, input_state: 'ndarray', expected_target: 'ndarray'):
        raise NotImplementedError()
