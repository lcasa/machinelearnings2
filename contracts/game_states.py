class GameState:

    def player_count(self) -> int:
        raise NotImplementedError()

    def get_active_player_id(self) -> int:
        raise NotImplementedError()

    def get_information_set_for_player(self, player_id: int) -> 'InformationSet':
        raise NotImplementedError()

    def get_available_actions_for_player(self, player_id: int) -> 'dict':
        raise NotImplementedError()

    def next(self, player_id: int, action_value):
        raise NotImplementedError()

    def is_game_over(self) -> bool:
        raise NotImplementedError()

    def get_current_scores(self) -> 'dict':
        raise NotImplementedError()

    def copy_game_state(self) -> 'GameState':
        raise NotImplementedError()

    def reset(self):
        raise NotImplementedError()
