from numpy.core.multiarray import ndarray

from machinelearnings2.contracts.game_states import GameState


class InformationSet:

    def __hash__(self) -> int:
        raise NotImplementedError()

    def __eq__(self, other):
        raise NotImplementedError()

    def __ne__(self, other):
        raise NotImplementedError()

    def determinize(self) -> 'GameState':
        raise NotImplementedError()

    def vectorize(self) -> 'ndarray':
        raise NotImplementedError()

    def __str__(self):
        raise NotImplementedError()
