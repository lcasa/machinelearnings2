class Runner:

    def run(self, max_round) -> 'dict':
        raise NotImplementedError()
