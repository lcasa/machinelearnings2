class ball:
    def __init__(self):
        self.x = 0
        self.y = 0
        self.velocity = (1, -1)
        self.angle = 270

    def newPos(self)->(int, int):
        return self.x + self.velocity[0], self.y + self.velocity[1]

    def copy(self) -> 'ball':
        nball = ball()
        nball.x = self.x
        nball.y = self.y
        nball.velocity = self.velocity
        nball.angle = self.angle
        return nball
