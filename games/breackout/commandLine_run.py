from machinelearnings2.agents.command_line_agents import CommandLineAgent
from machinelearnings2.games.breackout.game_states import BreakOut_GameSate
from machinelearnings2.runners.basic_runner import BasicRunner

if __name__ == "__main__":
    gameRunner = BasicRunner(BreakOut_GameSate(), [CommandLineAgent()])

    print(gameRunner.run(1))
