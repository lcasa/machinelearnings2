import time

from machinelearnings2.agents.vanilla_deep_q_learning_agents import VanillaDeepQLearningAgent
from machinelearnings2.games.breackout.game_states import BreakOut_GameSate
from machinelearnings2.runners.basic_runner import BasicRunner

if __name__ == "__main__":
    gameRunner = BasicRunner(BreakOut_GameSate(), [
        VanillaDeepQLearningAgent(9600, 3)])

    for i in range(100000):
        start_time = time.time()
        print("round " + str(i+1))
        print(gameRunner.run(10))
        print("time elapsed: {:.2f}s".format(time.time() - start_time))
