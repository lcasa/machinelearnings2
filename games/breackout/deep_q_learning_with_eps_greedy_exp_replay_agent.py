import time

from machinelearnings2.agents.deep_q_learning_with_eps_greedy_exp_replay_agents import \
    DeepQLearningWithEpsGreedyExpReplayAgent
from machinelearnings2.games.breackout.game_states import BreakOut_GameSate
from machinelearnings2.runners.basic_runner import BasicRunner

if __name__ == "__main__":
    gameRunner = BasicRunner(BreakOut_GameSate(), [
        DeepQLearningWithEpsGreedyExpReplayAgent(9600, 3)])

    for i in range(1000):
        start_time = time.time()
        print("round " + str(i+1))
        print(gameRunner.run(10))
        print(gameRunner.original_available_agents[0].epsilon)
        print("time elapsed: {:.2f}s".format(time.time() - start_time))
