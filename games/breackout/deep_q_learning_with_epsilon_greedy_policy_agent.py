import time

from machinelearnings2.agents.vanilla_deep_q_learning_with_epsilon_greedy_policy_agents import \
    VanillaDeepQLearningWithEpsilonGreedyPolicyAgent
from machinelearnings2.games.breackout.game_states import BreakOut_GameSate
from machinelearnings2.runners.basic_runner import BasicRunner

if __name__ == "__main__":
    gameRunner = BasicRunner(BreakOut_GameSate(), [
        VanillaDeepQLearningWithEpsilonGreedyPolicyAgent(9600, 3)])

    for i in range(100000):
        start_time = time.time()
        print("round " + str(i+1))
        print(gameRunner.run(10))
        print(gameRunner.original_available_agents[0].epsilon)
        print("time elapsed: {:.2f}s".format(time.time() - start_time))

