from typing import Dict

import numpy as np

from machinelearnings2.contracts.game_states import GameState
from machinelearnings2.contracts.information_sets import InformationSet
from machinelearnings2.games.breackout.ball import ball


class BreakOut_GameSate(GameState):

    score: Dict[int, float]

    def __init__(self):
        self.active_player = 1

        self.board = np.zeros((20, 48))
        self.height = len(self.board)
        self.width = len(self.board[0])
        self.game_over = False
        self.score = {1: 0.0}
        self.ball = ball()
        self.ball.x = self.height-5
        self.ball.y = self.width//2
        self.plateforme = self.width//2
        self.init_board()
        self.life = 1
        self.nbrofbrick = 8*self.width/2;

    def init_board(self):
        for i in range(2,10):
            for j in range(self.width):
                self.board[i, j] = self.getbrick(i - 2)
        self.board[self.ball.x][self.ball.y]=8
        self.board[self.height-2][self.plateforme-1] = 9
        self.board[self.height-2][self.plateforme] = 9
        self.board[self.height-2][self.plateforme+1] = 9

    def getbrick(self, h: int)->'int':
        if 0<=h<=1:
            return self.getscore('r')
        if 2<=h<=3:
            return self.getscore('o')
        if 4<=h<=5:
            return self.getscore('g')
        if 6<=h<=7:
            return self.getscore('y')

    def getscore(self, case: str)-> int:
        if case == 'y':
            return 1
        if case == 'g':
            return 3
        if case == 'o':
            return 5
        if case == 'r':
            return 7

    def player_count(self) -> int:
        return 1

    def get_active_player_id(self) -> int:
        return 1

    def get_information_set_for_player(self, player_id: int) -> 'InformationSet':
        from machinelearnings2.games.breackout.information_sets import BreakOutInformationSet
        return BreakOutInformationSet(self)

    def get_available_actions_for_player(self, player_id: int) -> 'dict':
        available_action = dict()
        available_action[0] = 0
        if self.board[self.height-2][0] == 0:
            available_action[1] = 1

        if self.board[self.height-2][self.width-1] == 0:
            available_action[2] = 2
        return available_action

    def next(self, player_id: int, action_value):

        # déplacement plateforme
        if action_value == 1:
            if(self.plateforme+1<self.width-1):
                self.board[self.height-2][self.plateforme+1] = 0
            self.board[self.height-2][self.plateforme] = 0
            self.plateforme+=-2
            if(self.plateforme<=0):
                self.plateforme = 1
                self.board[self.height-2][self.plateforme-1] = 9
                self.board[self.height-2][self.plateforme] = 9
                self.board[self.height-2][self.plateforme+1] = 9
            else:
                self.board[self.height-2][self.plateforme] = 9
                if(self.plateforme-1>=0):
                    self.board[self.height-2][self.plateforme-1] = 9
        if action_value == 2:
            if(self.plateforme-1>-1):
                self.board[self.height-2][self.plateforme-1] = 0
            self.board[self.height-2][self.plateforme] = 0
            self.plateforme+=2
            if(self.plateforme>=self.width-1):
                self.plateforme = self.width-2
                self.board[self.height-2][self.plateforme-1] = 9
                self.board[self.height-2][self.plateforme] = 9
                self.board[self.height-2][self.plateforme+1] = 9
            else:
                self.board[self.height-2][self.plateforme] = 9
                if(self.plateforme+1<self.width-1):
                    self.board[self.height-2][self.plateforme+1] = 9
        # déplacement bille
        newBallPos = self.ball.newPos()
        if(self.height-1<=newBallPos[0]) :

            self.ball.x = newBallPos[0]
            self.ball.y = newBallPos[1]
            self.life += -1

            self.game_over = self.is_game_over()
        else:
            if(0>=newBallPos[0]) :
                self.ball.velocity = (-self.ball.velocity[0], self.ball.velocity[1])
                newBallPos = self.ball.newPos()
            if(0>=newBallPos[1]) :
                self.ball.velocity = (self.ball.velocity[0], -self.ball.velocity[1])
                newBallPos = self.ball.newPos()

            if(self.width-1<=newBallPos[1]) :
                self.ball.velocity = (self.ball.velocity[0], -self.ball.velocity[1])
                newBallPos = self.ball.newPos()
            if(self.height-2 == newBallPos[0]):
                if(newBallPos[1]==self.plateforme):
                    rand = np.random.random()
                    if(rand<0.5):
                        self.ball.velocity = (abs(self.ball.velocity[0]), self.ball.velocity[1])
                        newBallPos = self.ball.newPos()
                    else:
                        self.ball.velocity = (-abs(self.ball.velocity[0]), self.ball.velocity[1])
                        newBallPos = self.ball.newPos()
                elif(newBallPos[1]==self.plateforme-1):
                    self.ball.velocity = (-abs(self.ball.velocity[0]), self.ball.velocity[1])
                    newBallPos = self.ball.newPos()
                elif(newBallPos[1]==self.plateforme+1) :
                    self.ball.velocity = (abs(self.ball.velocity[0]), self.ball.velocity[1])
                    newBallPos = self.ball.newPos()

            # destruction brick
            if(0<self.board[newBallPos[0]][newBallPos[1]] <= 7 ):
                if(newBallPos[1]%2==0):
                    self.board[newBallPos[0]][newBallPos[1]+1] = 0
                else:
                    self.board[newBallPos[0]][newBallPos[1]-1] = 0
                self.score[self.active_player]+=self.board[newBallPos[0]][newBallPos[1]]
                self.board[newBallPos[0]][newBallPos[1]] = 0
                self.nbrofbrick = self.nbrofbrick-1
                # rebond
                self.ball.velocity = (-self.ball.velocity[0], self.ball.velocity[1])

            self.board[self.ball.x][self.ball.y]=0
            self.ball.x = newBallPos[0]
            self.ball.y = newBallPos[1]
            self.board[self.ball.x][self.ball.y]=8

            # end gamme
            self.game_over = self.is_game_over()
        """if self.ball.y == self.plateforme :#or self.ball.y == self.plateforme + 1 or self.ball.y == self.plateforme - 1
            self.score[self.active_player] += 1
"""
    def is_game_over(self) -> bool:
        if self.life <= 0:
            return True
        if self.nbrofbrick == 0:
            return True
        return False

    def get_current_scores(self) -> 'dict':
        score = self.score.copy()
        score[self.active_player] = float(score[self.active_player])/float(1*self.width+3*self.width+5*self.width+7*self.width)
        return score

    def copy_game_state(self) -> 'GameState':
        gs = BreakOut_GameSate()
        gs.board = self.board.copy()
        gs.score = self.score
        gs.nbrofbrick = self.nbrofbrick
        gs.game_over = self.game_over
        gs.ball = self.ball
        gs.life = self.life
        gs.width = self.width
        gs.height = self.height
        gs.active_player = self.active_player
        return gs

    def reset(self):
        self.__init__()
