import numpy as np

from numpy.core.multiarray import ndarray
from tensorflow.python.keras._impl.keras.utils import to_categorical

from machinelearnings2.contracts.information_sets import InformationSet


class BreakOutInformationSet(InformationSet):
    def __init__(self, gs: 'BreakOut_GameSate'):
        self.board = gs.board.copy()
        self.active_player = gs.active_player
        self.score = gs.score.copy()
        self.game_over = gs.game_over
        self.ball = gs.ball.copy()
        self.life = gs.life
        self.plateforme = gs.plateforme
        self.width = gs.width
        self.height = gs.height

    def __hash__(self) -> int:
        hash = 0
        hash += self.plateforme
        hash += self.ball.y*self.width
        #hash += self.ball.x*self.width*self.width
        return hash
    """
     posx=[0-47]
     hash =0 
        hash+= posx
    hash+=bposx*48
    hash+=bposy*48*48
    """

    def __eq__(self, other):
        return np.array_equal(other.board, self.board)

    def __ne__(self, other):
        return not np.array_equal(other.board, self.board)

    def determinize(self) -> 'GameState':
        from machinelearnings2.games.breackout.game_states import BreakOut_GameSate
        gs = BreakOut_GameSate()
        gs.score = self.score
        gs.active_player = self.active_player
        gs.game_over = self.game_over
        gs.board = self.board.copy()
        gs.ball = self.ball
        gs.life = self.life
        gs.height = self.height
        gs.width = self.width
        gs.plateforme = self.plateforme
        return gs

    def vectorize(self) -> 'ndarray':

        input_vector = to_categorical(np.reshape(self.board, (self.height*self.width,)), 10).flatten()
        return input_vector

    @staticmethod
    def _get_player_symbol(value: int)->'str':
        symbol = [" ","y"," ","g"," ","o"," ","r","b","_"]
        return symbol[value]

    def __str__(self):
        str_acc = ""
        for i in range(self.height):
            for j in range(self.width):
                str_acc += BreakOutInformationSet._get_player_symbol(int(self.board[i, j]))
            str_acc += "\n"
        return str_acc
