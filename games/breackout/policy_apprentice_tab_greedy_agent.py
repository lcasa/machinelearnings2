import time

from machinelearnings2.agents.policy_apprentice_agents import PolicyApprenticeAgent
from machinelearnings2.agents.tabular_q_learning_agents_with_epsilon_greedy_policy_agents import \
    TabularQLearningAgentWithEpsilonGreedyPolicy
from machinelearnings2.games.breackout.game_states import BreakOut_GameSate
from machinelearnings2.runners.basic_runner import BasicRunner

TabularQLearningAgentWithEpsilonGreedyPolicy

if __name__ == "__main__":
    gameRunner = BasicRunner(BreakOut_GameSate(),
                             [PolicyApprenticeAgent(
                                 TabularQLearningAgentWithEpsilonGreedyPolicy(),
                                 9600, 3, dataset_length=8196*16, warmup_time=4096*4096*4)]
                             )

    for i in range(1000):
        start_time = time.time()
        print("round " + str(i+1))
        print(gameRunner.run(1000))
        print("time elapsed: {:.2f}s".format(time.time() - start_time))
