from machinelearnings2.agents.random_agents import RandomAgent
from machinelearnings2.agents.random_rollouts_agents import RandomRolloutsAgent
from machinelearnings2.games.breackout.game_states import BreakOut_GameSate
from machinelearnings2.runners.basic_runner import BasicRunner
import time


# my code here


if __name__ == "__main__":


    gameRunner = BasicRunner(BreakOut_GameSate(), [RandomRolloutsAgent(200, [RandomAgent()])])
    start_time = time.time()
    print(gameRunner.run(100))
    print("time elapsed: {:.2f}s".format(time.time() - start_time))
