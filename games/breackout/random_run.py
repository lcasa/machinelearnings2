import time

from machinelearnings2.agents.random_agents import RandomAgent
from machinelearnings2.games.breackout.game_states import BreakOut_GameSate
from machinelearnings2.runners.basic_runner import BasicRunner

if __name__ == "__main__":
    start_time = time.time()
    gameRunner = BasicRunner(BreakOut_GameSate(), [RandomAgent()])

    print(gameRunner.run(1000))
    print("time elapsed: {:.2f}s".format(time.time() - start_time))

