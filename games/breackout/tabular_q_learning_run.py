import time

from machinelearnings2.agents.tabular_q_learning_agents import TabularQLearningAgent
from machinelearnings2.games.breackout.game_states import BreakOut_GameSate
from machinelearnings2.runners.basic_runner import BasicRunner

if __name__ == "__main__":
    gameRunner = BasicRunner(BreakOut_GameSate(), [TabularQLearningAgent()])

    for i in range(1000):
        start_time = time.time()
        print("round " + str(i+1))
        print(gameRunner.run(1000))
        print("time elapsed: {:.2f}s".format(time.time() - start_time))
