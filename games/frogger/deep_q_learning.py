from machinelearnings2.agents.vanilla_deep_q_learning_agents import VanillaDeepQLearningAgent
from machinelearnings2.games.frogger.game_states import FroggerGameState
from machinelearnings2.runners.basic_runner import BasicRunner

if __name__ == "__main__":
    gameRunner = BasicRunner(FroggerGameState(), [
        VanillaDeepQLearningAgent(168*4 + 168, 5)])

    for i in range(1000):
        print(gameRunner.run(1000))
