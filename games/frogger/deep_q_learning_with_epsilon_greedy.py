from machinelearnings2.agents.tabular_q_learning_agents_with_epsilon_greedy_policy_agents import \
    TabularQLearningAgentWithEpsilonGreedyPolicy
from machinelearnings2.agents.vanilla_deep_q_learning_with_epsilon_greedy_policy_agents import \
    VanillaDeepQLearningWithEpsilonGreedyPolicyAgent
from machinelearnings2.games.frogger.game_states import FroggerGameState
from machinelearnings2.runners.basic_runner import BasicRunner

if __name__ == "__main__":
    gameRunner = BasicRunner(FroggerGameState(), [
        VanillaDeepQLearningWithEpsilonGreedyPolicyAgent(168*4 + 168, 5),
        TabularQLearningAgentWithEpsilonGreedyPolicy()])

    for i in range(1000):
        print(gameRunner.run(1000))
        print(gameRunner.original_available_agents[0].epsilon)
