from machinelearnings2.agents.deep_q_learning_with_eps_greedy_exp_replay_agents import \
    DeepQLearningWithEpsGreedyExpReplayAgent
from machinelearnings2.agents.random_agents import RandomAgent, RandomAgentNonNumpy
from machinelearnings2.games.frogger.game_states import FroggerGameState
from machinelearnings2.games.tictactoe.game_states import TicTacToeGameState
from machinelearnings2.runners.basic_runner import BasicRunner

if __name__ == "__main__":
    gameRunner = BasicRunner(FroggerGameState(), [
        DeepQLearningWithEpsGreedyExpReplayAgent(168*4 + 168, 5)])

    for i in range(100000):
        print(gameRunner.run(10))
        print(gameRunner.original_available_agents[0].epsilon)
