from enum import Enum

import numpy as np

from machinelearnings2.contracts.game_states import GameState
from machinelearnings2.contracts.information_sets import InformationSet


class Moves(Enum):
    UP = 1
    LEFT = 2
    RIGHT = 3
    DOWN = 4
    NOTHING = 0


class FroggerGameState(GameState):

    def __init__(self):
        self.game_over = False
        self.board = np.zeros((12, 14))
        self.score = {1: 0}
        self.player_position = (11, 6)
        self.max_iter = 1000
        self.current_iter = 0
        # self.max_simu = 300
        self.simulation = False
        rand_range = 0.65
        # np.random.seed(25)

        for i in range(12):
            for j in range(14):
                suite = True
                if i == 11:
                    self.board[i, j] = 1
                    suite = False
                if i == 6:
                    self.board[i, j] = 1
                    suite = False
                if i == 0:
                    suite = False
                    if j % 3 == 0:
                        if j == 3:
                            self.board[i, j] = 3
                        else:
                            self.board[i, j] = 2
                    else:
                        self.board[i, j] = 0
                if suite and self.board[i, j] == 0 and np.random.random() <= rand_range:
                    self.board[i, j] = 1

    def player_count(self) -> int:
        return 1

    def get_active_player_id(self) -> int:
        return 1

    def get_information_set_for_player(self, player_id: int) -> 'InformationSet':
        from machinelearnings2.games.frogger.information_sets import FroggerInformationSet
        return FroggerInformationSet(self)

    def get_available_actions_for_player(self, player_id: int) -> 'dict':
        available_actions = {}
        (pi, pj) = self.player_position
        i = 0
        # available_actions[i] = Moves.NOTHING
        i += 1
        if (pi - 1) >= 0:
            available_actions[i] = Moves.UP
        i += 1
        if (pj - 1) >= 0:
            available_actions[i] = Moves.LEFT
        i += 1
        if (pj + 1) <= 13:
            available_actions[i] = Moves.RIGHT
        i += 1
        if (pi + 1) <= 11:
            available_actions[i] = Moves.DOWN

        return available_actions

    def next(self, player_id: int, action_value):
        assert not self.game_over
        assert isinstance(action_value, Moves)
        (pi, pj) = self.player_position
        if action_value == Moves.UP:  # Haut
            action_coords = (pi-1, pj)
        if action_value == Moves.LEFT:  # Gauche
            action_coords = (pi, pj-1)
        if action_value == Moves.RIGHT:  # Droite
            action_coords = (pi, pj+1)
        if action_value == Moves.DOWN:  # Bas
            action_coords = (pi+1, pj)
        if action_value == Moves.NOTHING:  # Rien à faire
            action_coords = self.player_position

        self.player_position = action_coords

        (terminal, win) = self._check_game_over(action_coords, action_value)

        self.game_over = terminal
        self.current_iter += 1

        # print(self.player_position[1])
        # print(self.score[1])
        self.score[1] = max(self.score[1], (11 - self.player_position[0]) / 11.0 * (0.99999 ** self.current_iter))
        if win != 0:
            self.score[1] += win
        self.score[1] /= 4
        # print(self.score)

    def _check_game_over(self, action_coords, action_value) -> (bool, int):
        # (ti, tj) = action_coords
        val = self.board[action_coords]
        if val == 2:
            return True, 1
        if val == 3:
            return True, val
        if val == 0:
            return True, 0
        if self.current_iter >= self.max_iter:
            return True, 0
        # if self.simulation is True and self.current_iter >= self.max_simu:
        #     return True, 0
        return False, 0

    def is_game_over(self) -> bool:
        return self.game_over

    def get_current_scores(self) -> 'dict':
        return self.score

    def copy_game_state(self) -> 'FroggerGameState':
        gs = FroggerGameState()
        gs.game_over = self.game_over
        gs.score = self.score
        gs.board = self.board.copy()
        gs.current_iter = self.current_iter
        gs.max_iter = self.max_iter
        return gs

    def reset(self):
        self.__init__()
