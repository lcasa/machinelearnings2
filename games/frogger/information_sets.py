import numpy as np

from machinelearnings2.contracts.game_states import GameState
from machinelearnings2.contracts.information_sets import InformationSet
from machinelearnings2.games.frogger.game_states import FroggerGameState


class FroggerInformationSet(InformationSet):

    def __init__(self, gs: 'FroggerGameState'):
        self.board = gs.board.copy()
        self.score = gs.score
        self.game_over = gs.game_over
        self.player_position = gs.player_position
        self.max = gs.max_iter
        self.current = gs.current_iter

    def __hash__(self) -> int:
        # A adapter quand le plateau changera, voir cahier
        (pi, pj) = self.player_position
        # hashcode = 0
        hashcode = pi + (pj * 12)
        return hashcode

    def __eq__(self, other):
        return np.array_equal(other.board, self.board) and other.player_position == self.player_position

    def __ne__(self, other):
        return not (np.array_equal(other.board, self.board) and other.player_position == self.player_position)

    def determinize(self) -> 'GameState':
        from machinelearnings2.games.frogger.game_states import FroggerGameState
        gs = FroggerGameState()
        gs.score = self.score
        gs.player_position = self.player_position
        gs.game_over = self.game_over
        gs.board = self.board.copy()
        gs.max_iter = self.max
        gs.current_iter = self.current
        return gs

    def vectorize(self) -> 'ndarray':
        input_vector = np.zeros((168*4 + 168,))

        for i, val in enumerate(np.reshape(self.board, (168,))):
            input_vector[i * 4 + int(self.board[i // 14, i % 14])] = 1.0

        input_vector[168*4 + self.player_position[0] * 14 + self.player_position[1]] = 1.0

        return input_vector

    @staticmethod
    def _get_symbol(case):
        if case == 0:
            return ' '
        if case == 1 or case == 2:
            return '_'
        if case == 3:
            return 'I'

        raise Exception("Case not recognized !")

    def __str__(self):
        str_acc = ''
        for i in range(12):
            for j in range(14):
                if (i, j) == self.player_position:
                    str_acc += 'X'
                else:
                    str_acc += FroggerInformationSet._get_symbol(self.board[i, j])
            str_acc += "\n"
        return str_acc
