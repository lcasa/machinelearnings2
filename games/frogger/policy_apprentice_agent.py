from machinelearnings2.agents.command_line_agents import CommandLineAgent
from machinelearnings2.agents.policy_apprentice_agents import PolicyApprenticeAgent
from machinelearnings2.agents.random_agents import RandomAgentNonNumpy
from machinelearnings2.agents.random_rollouts_agents import RandomRolloutsOptimistAgent
from machinelearnings2.agents.tabular_q_learning_agents_with_epsilon_greedy_policy_agents import \
    TabularQLearningAgentWithEpsilonGreedyPolicy
from machinelearnings2.games.frogger.game_states import FroggerGameState
from machinelearnings2.runners.basic_runner import BasicRunner

if __name__ == "__main__":
    gameRunner = BasicRunner(FroggerGameState(),
                             [PolicyApprenticeAgent(
                                TabularQLearningAgentWithEpsilonGreedyPolicy(),
                                 168*4 + 168, 5, dataset_length=4096*16, warmup_time=4096*10
                             )])

    for i in range(1000):
        print(gameRunner.run(1000))

# if __name__ == "__main__":
#     gameRunner = BasicRunner(FroggerGameState(),
#                              [PolicyApprenticeAgent(
#                                  RandomRolloutsOptimistAgent(10, [RandomAgentNonNumpy()]),
#                                  168*4 + 168, 5, dataset_length=4096
#                              )])
#
#     for i in range(1000):
#         print(gameRunner.run(100))
