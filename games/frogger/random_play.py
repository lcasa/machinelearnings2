from machinelearnings2.agents.random_agents import RandomAgent, RandomAgentNonNumpy
from machinelearnings2.games.frogger.game_states import FroggerGameState
from machinelearnings2.runners.basic_runner import BasicRunner

if __name__ == "__main__":
    gameRunner = BasicRunner(FroggerGameState(), [RandomAgentNonNumpy()])

    print(gameRunner.run(10000))
