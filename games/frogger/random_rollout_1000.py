from machinelearnings2.agents.random_agents import RandomAgent, RandomAgentNonNumpy
from machinelearnings2.agents.random_rollouts_agents import RandomRolloutsAgent, RandomRolloutsOptimistAgent
from machinelearnings2.games.frogger.game_states import FroggerGameState
from machinelearnings2.runners.basic_runner import BasicRunner

if __name__ == "__main__":
    gameRunner = BasicRunner(FroggerGameState(), [RandomRolloutsOptimistAgent(
        100, [RandomAgentNonNumpy()]
    )])

    print(gameRunner.run(100))
