from machinelearnings2.agents.tabular_q_learning_agents import TabularQLearningAgent
from machinelearnings2.games.frogger.game_states import FroggerGameState
from machinelearnings2.runners.basic_runner import BasicRunner

if __name__ == "__main__":
    gameRunner = BasicRunner(FroggerGameState(), [TabularQLearningAgent()])

    for i in range(1000):
        print(gameRunner.run(1000))
