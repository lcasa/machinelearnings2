from machinelearnings2.agents.deep_q_learning_with_eps_greedy_exp_replay_agents import \
    DeepQLearningWithEpsGreedyExpReplayAgent
from machinelearnings2.agents.tabular_q_learning_agents_with_epsilon_greedy_policy_agents import \
    TabularQLearningAgentWithEpsilonGreedyPolicy
from machinelearnings2.games.tictactoe.game_states import TicTacToeGameState
from machinelearnings2.runners.basic_runner import BasicRunner

if __name__ == "__main__":
    gameRunner = BasicRunner(TicTacToeGameState(), [
        DeepQLearningWithEpsGreedyExpReplayAgent(9 * 3, 9),
        TabularQLearningAgentWithEpsilonGreedyPolicy()])

    for i in range(1000):
        print(gameRunner.run(100))
        print(gameRunner.original_available_agents[0].epsilon)
