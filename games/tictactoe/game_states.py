import numpy as np

from machinelearnings2.contracts.game_states import GameState
from machinelearnings2.contracts.information_sets import InformationSet


class TicTacToeGameState(GameState):

    def __init__(self):
        self.active_player = 1
        self.board = np.zeros((3, 3))
        self.game_over = False
        self.score = {1: 0, 2: 0}

    def player_count(self) -> int:
        return 2

    def get_active_player_id(self) -> int:
        return self.active_player

    def get_information_set_for_player(self, player_id: int) -> 'InformationSet':
        from machinelearnings2.games.tictactoe.information_sets import TicTacToeInformationSet
        return TicTacToeInformationSet(self)

    def get_available_actions_for_player(self, player_id: int) -> 'dict':
        available_actions = {}
        for i, val in enumerate(self.board.reshape((9,))):
            if val == 0:
                available_actions[i] = i
        return available_actions

    def next(self, player_id: int, action_value: int):
        assert not self.game_over
        assert player_id == self.active_player
        assert 0 <= action_value <= 8
        action_coords = action_value // 3, action_value % 3
        assert self.board[action_coords] == 0
        self.board[action_coords] = player_id
        (winner, terminal) = self._check_game_over(action_coords)

        self.game_over = terminal
        other_player = self._other_player()

        if winner != 0:
            self.score[self.active_player] = 1
            self.score[other_player] = 0
        elif terminal:
            self.score[self.active_player] = 0.5
            self.score[other_player] = 0.5

        self.active_player = other_player

    def _other_player(self) -> int:
        return self.active_player % 2 + 1

    def _check_game_over(self, action_coords) -> (int, bool):
        (ti, tj) = action_coords

        if self.board[ti, 0] == self.board[ti, 1] == self.board[ti, 2]:
            return self.active_player, True

        if self.board[0, tj] == self.board[1, tj] == self.board[2, tj]:
            return self.active_player, True

        if self.board[0, 0] == self.board[1, 1] == self.board[2, 2] == self.active_player:
            return self.active_player, True

        if self.board[2, 0] == self.board[1, 1] == self.board[0, 2] == self.active_player:
            return self.active_player, True

        for val in self.board.flatten():
            if val == 0:
                return 0, False

        return 0, True

    def is_game_over(self) -> bool:
        return self.game_over

    def get_current_scores(self) -> 'dict':
        return self.score.copy()

    def copy_game_state(self) -> 'TicTacToeGameState':
        gs = TicTacToeGameState()
        gs.score = self.score.copy()
        gs.active_player = self.active_player
        gs.game_over = self.game_over
        gs.board = self.board.copy()
        return gs

    def reset(self):
        self.__init__()
