import numpy as np
from numpy.core.multiarray import ndarray
from tensorflow.python.keras._impl.keras.utils import to_categorical
from machinelearnings2.contracts.game_states import GameState
from machinelearnings2.contracts.information_sets import InformationSet
from machinelearnings2.games.tictactoe.game_states import TicTacToeGameState


class TicTacToeInformationSet(InformationSet):

    def __init__(self, gs: 'TicTacToeGameState'):
        self.board = gs.board.copy()
        self.active_player = gs.active_player
        self.score = gs.score
        self.game_over = gs.game_over

    def __hash__(self) -> int:
        return hash(self.board.tobytes())

    def __eq__(self, other):
        return np.array_equal(other.board, self.board)

    def __ne__(self, other):
        return not np.array_equal(other.board, self.board)

    def determinize(self) -> 'GameState':
        from machinelearnings2.games.tictactoe.game_states import TicTacToeGameState
        gs = TicTacToeGameState()
        gs.score = self.score
        gs.active_player = self.active_player
        gs.game_over = self.game_over
        gs.board = self.board.copy()
        return gs

    def vectorize(self) -> 'ndarray':
        vectorized_state = to_categorical(np.reshape(self.board, (9,)), 3)
        return np.reshape(vectorized_state, (9 * 3,))

    @staticmethod
    def _get_player_symbol(player_id):
        if player_id == 0:
            return '_'

        if player_id == 1:
            return 'X'

        if player_id == 2:
            return 'O'

        raise Exception("Player ID not recognized !")

    def __str__(self):
        str_acc = "active player : " + str(self.active_player) + "\n"

        for i in range(3):
            for j in range(3):
                str_acc += TicTacToeInformationSet._get_player_symbol(self.board[i, j])
            str_acc += "\n"
        return str_acc
