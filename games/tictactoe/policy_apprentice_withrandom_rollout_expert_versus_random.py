from machinelearnings2.agents.policy_apprentice_agents import PolicyApprenticeAgent
from machinelearnings2.agents.random_agents import RandomAgent
from machinelearnings2.agents.random_rollouts_agents import RandomRolloutsAgent
from machinelearnings2.games.tictactoe.game_states import TicTacToeGameState
from machinelearnings2.runners.basic_runner import BasicRunner

if __name__ == "__main__":
    gameRunner = BasicRunner(TicTacToeGameState(),
                             [PolicyApprenticeAgent(
                                 RandomRolloutsAgent(10, [RandomAgent(), RandomAgent()]),
                                 9 * 3, 9, dataset_length=4096

                             ),

                                 RandomAgent()])

    for i in range(1000):
        print(gameRunner.run(100))
