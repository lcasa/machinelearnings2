from machinelearnings2.agents.random_agents import RandomAgent
from machinelearnings2.games.tictactoe.game_states import TicTacToeGameState
from machinelearnings2.runners.basic_runner import BasicRunner

if __name__ == "__main__":
    gameRunner = BasicRunner(TicTacToeGameState(), [RandomAgent(), RandomAgent()])

    print(gameRunner.run(10000))
