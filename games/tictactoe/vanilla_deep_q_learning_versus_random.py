from machinelearnings2.agents.random_agents import RandomAgent
from machinelearnings2.agents.vanilla_deep_q_learning_agents import VanillaDeepQLearningAgent
from machinelearnings2.games.tictactoe.game_states import TicTacToeGameState
from machinelearnings2.runners.basic_runner import BasicRunner

if __name__ == "__main__":
    gameRunner = BasicRunner(TicTacToeGameState(), [
        VanillaDeepQLearningAgent(9*3, 9),
        RandomAgent()])

    for i in range(1000):
        print(gameRunner.run(100))
