from machinelearnings2.agents.tabular_q_learning_agents import TabularQLearningAgent
from machinelearnings2.agents.vanilla_deep_q_learning_with_epsilon_greedy_policy_agents import \
    VanillaDeepQLearningWithEpsilonGreedyPolicyAgent
from machinelearnings2.games.tictactoe.game_states import TicTacToeGameState
from machinelearnings2.runners.basic_runner import BasicRunner

if __name__ == "__main__":
    gameRunner = BasicRunner(TicTacToeGameState(), [
        VanillaDeepQLearningWithEpsilonGreedyPolicyAgent(9 * 3, 9),
        TabularQLearningAgent()])

    for i in range(1000):
        print(gameRunner.run(100))
        print(gameRunner.original_available_agents[0].epsilon)
