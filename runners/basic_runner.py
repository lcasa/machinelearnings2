from collections import deque
from time import sleep
from typing import Iterable

from machinelearnings2.contracts.agents import Agent
from machinelearnings2.contracts.game_states import GameState
from machinelearnings2.contracts.runners import Runner


class BasicRunner(Runner):

    def __init__(self, gs: GameState, agent_pool: 'Iterable[Agent]', reset: bool = True):
        self.gs = gs
        self.reset = reset
        self.original_available_agents = list(agent_pool)

    def run(self, max_round) -> 'dict':

        accumulated_scores = dict()
        for round_id in range(max_round):
            if self.reset:
                self.gs.reset()
                gs = self.gs
            else:
                gs = self.gs.copy_game_state()
            playing_agents = dict()
            idle_agents = deque(self.original_available_agents)

            while not gs.is_game_over():
                active_player = gs.get_active_player_id()

                old_scores = gs.get_current_scores().copy()
                if active_player not in playing_agents:
                    playing_agents[active_player] = idle_agents.popleft()

                infoset = gs.get_information_set_for_player(
                    active_player)

                # if self.reset:
                #     print(infoset)
                #     sleep(1)
                available_actions = gs.get_available_actions_for_player(active_player)
                chosen_action_value = playing_agents[active_player].act(active_player,
                                                                        infoset,
                                                                        available_actions)
                # if self.reset:
                #     print(chosen_action_value)
                gs.next(active_player, chosen_action_value)
                new_scores = gs.get_current_scores()
                terminal = gs.is_game_over()
                rewards = dict()
                # if self.reset:
                #     print(terminal)
                for i in playing_agents.keys():
                    rewards[i] = new_scores[i] - (0 if i not in old_scores else old_scores[i])

                for i in playing_agents.keys():
                    playing_agents[i].inform_action_result(i, rewards, new_scores, terminal)

            round_score = gs.get_current_scores()
            for i in playing_agents.keys():
                if i not in accumulated_scores:
                    accumulated_scores[i] = 0
                accumulated_scores[i] += round_score[i]

        return accumulated_scores
